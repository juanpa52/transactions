# Transaction system with Java and MongoDB

## How to run:

> Requirements to run: 
> *  Docker 
> *  Maven
> *  Java 1.8 or higher

1. Clone repository 
2. Run mongodb with docker
    * `docker volume create mongo-datadb`
    * `docker run --name db -d -v mongo-datadb:/data/db -p 27017:27017 mongo`
3. Perfom the persist operations using the generated jar file like next:
    * `java -jar target/transactionSys-1.0.jar <commands>`
      * `commands: `
        * **Add transaction** `<userId> add <json_payload> `
          * This command accept either formatted or raw payload
          * `345 add {"amount": 1.15,"description": "Joes Tacos 1","date": "2018-12-30"}`
          * **Response** 
                      `{
                         "userId" : 345,
                         "transactionId" : "5d7b457eba45d25560e37d38",
                         "description" : "Joes Tacos 1",
                         "amount" : 1.15,
                         "date" : "2018-12-30"
                       }`
        * **Show transaction** `<userId> add <transctionId> ` 
          * `345 5d7b457eba45d25560e37d38`
          * **Response** 
                               `{
                                  "userId" : 345,
                                  "transactionId" : "5d7b457eba45d25560e37d38",
                                  "description" : "Joes Tacos 1",
                                  "amount" : 1.15,
                                  "date" : "2018-12-30"
                                }` 
        * **List transactions** `<userId> list `
          * `345 list`
          * **Response**
                                 `[ {
                                    "userId" : 345,
                                    "transactionId" : "5d7b457eba45d25560e37d38",
                                    "description" : "Joes Tacos 1",
                                    "amount" : 1.15,
                                    "date" : "2018-12-30"
                                  }, {
                                    "userId" : 345,
                                    "transactionId" : "5d7b464dba45d203b4e53de9",
                                    "description" : "Joes Tacos 2",
                                    "amount" : 2.5,
                                    "date" : "2018-10-30"
                                  }, {
                                    "userId" : 345,
                                    "transactionId" : "5d7b465eba45d25480e51fca",
                                    "description" : "Joes Tacos 3",
                                    "amount" : 3.5,
                                    "date" : "2018-07-30"
                                  } ]` 
        * **Sum transactions** `<userId> sum ` 
          * `345 sum`
          * **Response**
                                  `{
                                     "userId" : 345,
                                     "sum" : 7.15
                                   }` 

## How to run the app with docker file if the app was running without finishing in each setence:
> Requirements to run: 
> *  Docker 
> *  Docker Compose

1. Looks for the docker-compose file on \transactions\docker-compose.yml and open a terminal in this path.
2. Run the next command:
* `docker-compose up -d`


