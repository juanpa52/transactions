#Getting base image
FROM alpine:edge
#Author
MAINTAINER juanpa52 <pablo_52junio@hotmail.com>
RUN apk add --no-cache openjdk8
COPY ["/target/transactionSys-1.0.jar","/usr/src/"]
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar","/usr/src/transactionSys-1.0.jar"]
EXPOSE 8080

