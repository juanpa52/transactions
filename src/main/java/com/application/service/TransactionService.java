package com.application.service;

import com.application.dao.TransactionDAO;
import com.application.entity.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TransactionService {

    private final TransactionDAO transactionDAO;
    private final ObjectMapper mapper;

    public TransactionService(TransactionDAO transactionDAO) {
        this.transactionDAO = transactionDAO;
        this.mapper = new ObjectMapper();
    }

    public Transaction addTransaction(Integer userId, String transactionPayload) {
        try {
            transactionPayload = convertStringToJsonString(transactionPayload);
            Transaction transaction = mapper.readValue(transactionPayload, Transaction.class);
            transaction.setUserId(userId);
            return transactionDAO.save(transaction);
        } catch (JsonProcessingException e) {
            System.out.println("AddTransaction operation failed trying to deserialize the payload");
            return new Transaction();
        }
    }

    public Transaction showTransaction(Integer userId, String transactionId) {
        return transactionDAO.getByUserIdAndTransactionId(userId, transactionId);
    }

    public List<Transaction> listTransactions(Integer userId) {
        List<Transaction> transactions = transactionDAO.getAllByUserId(userId);
        Collections.sort(transactions, (transactionA, transactionB) -> transactionB.getDate().compareTo(transactionA.getDate()));
        return transactions;
    }

    public Float sumTransactions(Integer userId) {
        List<Transaction> transactions = transactionDAO.getAllByUserId(userId);
        return transactions.stream().map(Transaction::getAmount).reduce(0.0F, Float::sum);
    }

    private String convertStringToJsonString(String value){
        final String keyValuePattern = "(\\w+):\\s+([\\s\\w\\-\\.]+),*?";
        return value.replaceAll(keyValuePattern,"\"$1\":\"$2\"");
    }
}
