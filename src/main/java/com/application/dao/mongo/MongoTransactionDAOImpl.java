package com.application.dao.mongo;

import com.application.dao.TransactionDAO;
import com.application.entity.Transaction;
import com.application.entity.exception.TransactionFailedException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;

public class MongoTransactionDAOImpl implements TransactionDAO {

    public MongoTransactionDAOImpl transactionMongoDAO;

    private final MongoClient client = MongoFactory.getInstance();
    private final MongoDatabase database;
    private final MongoCollection<Transaction> transactionCollection;
    private final ObjectMapper mapper;

    public MongoTransactionDAOImpl() {
        database = client.getDatabase("TransactionSystem");
        transactionCollection = database.getCollection("transactions", Transaction.class);
        mapper = new ObjectMapper();
    }

    @Override
    public Transaction save(Transaction transaction) {
        if (transaction.getId() == null) {
            transaction.setId(new ObjectId());
        }
        transactionCollection.insertOne(transaction);
        return transaction;
    }

    @Override
    public List<Transaction> getAllByUserId(Integer userId) {
        Document queryDocument = new Document();
        queryDocument.put("userId", userId);
        return transactionCollection.find(queryDocument, Transaction.class).into(new ArrayList<>());
    }

    @Override
    public Transaction getByUserIdAndTransactionId(Integer userId, String transactionId) {
        Document queryDocument = new Document();
        queryDocument.put("transactionId", transactionId);
        queryDocument.put("userId", userId);
        return transactionCollection.find(queryDocument, Transaction.class).first();
    }

    @Override
    public Transaction updateById(String id, Transaction transaction) throws TransactionFailedException {
        Document queryDocument = new Document();
        ObjectId objectId = new ObjectId(id);
        queryDocument.put("_id", objectId);

        Document updateDocument = null;
        try {
            updateDocument = Document.parse(mapper.writeValueAsString(transaction));
        } catch (JsonProcessingException e) {
            throw new TransactionFailedException("Exception deserializing transaction entity", e);
        }

        Document updateSintaxisDoc = new Document();
        updateSintaxisDoc.put("$set", updateDocument);

        UpdateResult result = transactionCollection.updateOne(queryDocument, updateSintaxisDoc);
        if (!result.wasAcknowledged()) {
            throw new TransactionFailedException("Update transaction was not succesfull");
        }
        return getByUserIdAndTransactionId(transaction.getUserId(),transaction.getTransactionId());
    }

    @Override
    public boolean deleteById(String id) {
        Document queryDocument = new Document();
        ObjectId objectId = new ObjectId(id);
        queryDocument.put("_id", objectId);
        DeleteResult result = transactionCollection.deleteOne(queryDocument);
        return result.wasAcknowledged();
    }

}
