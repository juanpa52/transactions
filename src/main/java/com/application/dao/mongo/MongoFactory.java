package com.application.dao.mongo;

import com.mongodb.*;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.util.ArrayList;
import java.util.List;

public class MongoFactory {
    private static MongoClient client;

    private MongoFactory() {};

    public static MongoClient getInstance() {
        if (client == null) {
            client = getMongoClient();
        }
        return client;
    }

    private static MongoClient getMongoClient() {
        List<MongoCredential> credentials = new ArrayList<>();
        credentials.add(MongoCredential.createCredential("username", "database", new char[10]));

        CodecRegistry pojoCodecRegistry = CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), //default codec registry
                CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build())); //from Providers

        String host = System.getenv("HOST");
        if (host == null) {
            host = "localhost";
        }

        client = new MongoClient(new ServerAddress(host, 27017),
                //credentials,
                MongoClientOptions.builder()
                        .serverSelectionTimeout(30000) //number of milliseconds the mongo driver will wait to select a server for an operation before giving up and raising an error.
                        .connectTimeout(10000) //number of milliseconds the driver will wait before a new connection attempt is aborted.
                        .socketTimeout(3000) //number of milliseconds a send or receive on a socket can take before timeout.
                        .codecRegistry(pojoCodecRegistry)
                        .build());
        return client;
    }

    public static void closeClient() {
        client.close();
    }

}
