package com.application.dao;

import com.application.entity.Transaction;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

public interface TransactionDAO {

    Transaction save(Transaction transaction);

    Transaction getByUserIdAndTransactionId(Integer userId, String transactionId);

    List<Transaction> getAllByUserId(Integer userId);

    Transaction updateById(String id, Transaction transaction) throws JsonProcessingException;

    boolean deleteById(String id);

}
