package com.application.entity.exception;

public class TransactionFailedException extends RuntimeException {

    public TransactionFailedException(String msg) {
        super(msg);
    }

    public TransactionFailedException(String msg, Throwable t) {
        super(msg, t);
    }

}
