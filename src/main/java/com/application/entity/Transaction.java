package com.application.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.bson.codecs.pojo.annotations.BsonDiscriminator;
import org.bson.codecs.pojo.annotations.BsonProperty;
import org.bson.types.ObjectId;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@BsonDiscriminator
public class Transaction {

    //A BSON ObjectID is a 12-byte value consisting of:
    // a 4-byte timestamp (seconds since epoch)
    // a 3-byte machine id
    // a 2-byte process id
    // a 3-byte counter
    private ObjectId id;
    private Integer userId;
    private String transactionId;
    private String description;
    private Float amount;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date date;

    @BsonProperty
    @JsonProperty
    public void setTransactionId(String transactionId) {
        //If the entity already has an id, read it from the id object
        if (id != null) {
            //This applies when the id is saved on the DB
            this.transactionId = id.toHexString();
        } else {
            //This applies only when the uid is loaded from external file
            if (transactionId.length() < 24) {
                //Assign a new ObjectId if the id string is less than 24 bytes length
                this.id = new ObjectId();
                this.transactionId = this.id.toHexString();
            } else {
                this.id = new ObjectId(transactionId);
                this.transactionId = transactionId;
            }
        }
    }

    @BsonProperty
    public String getTransactionId() {
        if (transactionId == null) {
            transactionId = id.toHexString();
        }
        return transactionId;
    }

    @JsonIgnore //Avoid serialization of this field
    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
