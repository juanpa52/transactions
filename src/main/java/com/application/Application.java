package com.application;

import com.application.dao.mongo.MongoFactory;
import com.application.dao.mongo.MongoTransactionDAOImpl;
import com.application.entity.Transaction;
import com.application.service.TransactionService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Application {
    private static final Pattern ADD_TRANSACTION_PATTERN = Pattern.compile("(\\d+)\\s+add\\s+(\\{[\\s\\w\\-\\.\\:\\,]+\\})");
    private static final Pattern SHOW_TRANSACTION_PATTERN = Pattern.compile("(\\d+)\\s+(\\w+)");
    private static final Pattern LIST_TRANSACTION_PATTERN = Pattern.compile("(\\d+)\\s+list");
    private static final Pattern SUM_TRANSACTION_PATTERN = Pattern.compile("(\\d+)\\s+sum");

    private static List<Pattern> patterns = new LinkedList<>();

    static {
        patterns.addAll(Arrays.asList(ADD_TRANSACTION_PATTERN, LIST_TRANSACTION_PATTERN, SUM_TRANSACTION_PATTERN, SHOW_TRANSACTION_PATTERN));
    }

    public static void main(String[] args) {
        final String inputCommands = String.join(" ", args).replaceAll("\\s{2,}", " ");
        final ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
        final TransactionService transactionService = new TransactionService(new MongoTransactionDAOImpl());

        //Apply the respective pattern to find the correct match
        for (Pattern pattern : patterns) {
            String operationResult = "The operation was not existent " + Arrays.toString(args);
            Matcher matcher = pattern.matcher(inputCommands);
            if (matcher.matches()) {
                try {
                    if (pattern.equals(ADD_TRANSACTION_PATTERN)) {
                        final Integer userId = Integer.parseInt(matcher.group(1));
                        final String payload = matcher.group(2);
                        Transaction transaction = transactionService.addTransaction(userId, payload);
                        operationResult = mapper.writeValueAsString(transaction);

                    } else if (pattern.equals(SHOW_TRANSACTION_PATTERN)) {
                        final Integer userId = Integer.parseInt(matcher.group(1));
                        final String transactionId = matcher.group(2);
                        Transaction transaction = transactionService.showTransaction(userId, transactionId);
                        if (transaction != null) {
                            operationResult = mapper.writeValueAsString(transaction);
                        } else {
                            operationResult = "Transaction not found";
                        }

                    } else if (pattern.equals(LIST_TRANSACTION_PATTERN)) {
                        final Integer userId = Integer.parseInt(matcher.group(1));
                        List<Transaction> transactions = transactionService.listTransactions(userId);
                        operationResult = mapper.writeValueAsString(transactions);

                    } else if (pattern.equals(SUM_TRANSACTION_PATTERN)) {
                        final Integer userId = Integer.parseInt(matcher.group(1));
                        Map<String, Object> resultsMap = new LinkedHashMap<>();
                        Float sum = transactionService.sumTransactions(userId);
                        resultsMap.put("userId", userId);
                        resultsMap.put("sum", sum);
                        operationResult = mapper.writeValueAsString(resultsMap);

                    } else {
                        operationResult = "The input parameteres don't match with any knwown method";
                    }
                } catch (JsonProcessingException e) {
                    operationResult = "Error serializing object";
                }
                System.out.println(operationResult);
                break;
            }
        }

        MongoFactory.closeClient();
    }

}
